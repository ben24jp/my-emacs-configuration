(defun company-emacs-lisp-mode ()
  "Set up `company-mode' for `emacs-lisp-mode'."
  (set (make-local-variable 'company-backends)
       '((company-yasnippet
          company-capf
          company-elisp
          company-dabbrev-code
          company-files))))

(defun company-rustic-mode ()
  "Set up `company-mode' for `rustic-mode'."
  (set (make-local-variable 'company-backends)
       '((company-yasnippet
          company-capf
          company-keywords
          company-dabbrev-code
          company-files))))

(defun company-cc-mode ()
  "Set up `company-mode' for `c-common-mode'."
  (set (make-local-variable 'company-backends)
       '((company-yasnippet
          company-capf
          company-keywords
          company-dabbrev-code
          company-files))))

(add-hook 'emacs-lisp-mode-hook 'company-emacs-lisp-mode)
(add-hook 'rustic-mode-hook 'company-rustic-mode)
(add-hook 'c-mode 'company-cc-mode)
(add-hook 'c++-mode 'company-cc-mode)

(provide 'company-local-rcp)
