(use-package which-key
  :ensure t
  :defer t
  :init
  (setq which-key-sort-order #'which-key-key-order-alpha
        which-key-sort-uppercase-first nil
        which-key-add-column-padding 1
        which-key-max-display-columns nil
        which-key-min-display-lines 6
        which-key-side-window-slot -10)
  :hook (doom-first-input . which-key-mode)
  :config
  (setq which-key-idle-delay 0.1))
(which-key-mode)
(provide 'which-key-rcp)
