(use-package lsp-haskell
  :ensure t
  :defer t
  :config
  (add-hook 'haskell-mode-hook #'lsp)
  (add-hook 'haskell-doc-mode #'lsp)
  (add-hook 'haskell-literate-mode #'lsp))
(provide 'haskell-rcp)
