(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "M-s") 'flyspell-correct-at-point)
(provide 'keybinds-rcp)
