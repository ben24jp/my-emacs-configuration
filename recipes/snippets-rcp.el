(use-package yasnippet-snippets                          
  :ensure t                                              
  :defer t)                                              
                                                         
(use-package yasnippet                                   
  :ensure t                                              
  :defer t                                               
  :init (setq yas-snippet-dirs '("~/.config/emacs/snippets"))
  :config
  (add-hook 'prog-mode-hook #'yas-minor-mode)
  (yas-reload-all))

(provide 'snippets-rcp)
