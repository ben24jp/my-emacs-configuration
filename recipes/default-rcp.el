(setq inhibit-startup-message t)        ;; Disable startup messages
(scroll-bar-mode -1)                    ;; Disable the scroll bar
(tool-bar-mode -1)                      ;; Disable the tool bar
(tooltip-mode -1)                       ;; Disable tooltips
(set-fringe-mode 5)                     ;; Give some breathing room
(menu-bar-mode -1)                      ;; Diable menubar
(recentf-mode 0)
(electric-pair-mode 1)                  ;; Enable automatically adding pair brackets

(use-package rainbow-delimiters         ;; Enable color for pair brackets
  :ensure t
  :defer t
  :hook ((prog-mode org-src-mode) . rainbow-delimiters-mode)
  :config (rainbow-delimiters-mode t))

(use-package column-enforce-mode
  :ensure t
  :defer t
  :hook (prog-mode . column-enforce-mode)
  :config (80-column-rule))

(setq backup-directory-alist '(("." . "~/MyEmacsBackups")))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

(setq mouse-wheel-scroll-amount '(2 ((shift) . 1))) ;; Five line at a time
(setq mouse-wheel-progressive-speed nil)            ;; don't accelarate scrolling
(setq mouse-wheel-follow-mouse 't)                  ;; scroll window under mouse
(setq scroll-step 1)                                ;; Keyboard scrolling set to one line at a time

(column-number-mode 1)
(global-display-line-numbers-mode t)
(setq display-line-numbers-type 'relative)
;; Enable line numbers in specifiv modes
(dolist (mode '(text-mode-hook
    prog-mode-hook
    cond-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(setq-default indent-tabs-mode nil
    tab-width 4)

(setq-default tab-always-indent nil)

(setq tabify-regexp "^\t* [ \t]+")

(fset 'yes-or-no-p 'y-or-n-p)

(setq require-final-newline t)

(setq next-line-add-newlines nil)

(setq message-log-max 100)

(require 'paren) (show-paren-mode t)

(provide 'default-rcp)
