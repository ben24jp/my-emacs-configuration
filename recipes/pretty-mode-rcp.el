(setq pretty-activate-groups
 '(:equality :arrows :arrows-towheaded :logic :sets :sets-operations :arithmetic))
:config
(global-pretty-mode 1)

(provide 'pretty-mode-rcp)
