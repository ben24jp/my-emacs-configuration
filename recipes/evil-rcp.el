(use-package undo-tree
  :ensure t
  :init
  (setq evil-undo-system 'undo-tree)
  :config
  (setq undo-limit 400000           ; 400kb (default is 160kb)
        undo-strong-limit 3000000   ; 3mb   (default is 240kb)
        undo-outer-limit 48000000)  ; 48mb  (default is 24mb)
  (global-undo-tree-mode t))

(use-package evil
  :ensure t
  :defer t
  :init
  (setq evil-want-integration t)
  (setq evil-want-fine-undo t)
  (setq evil-want-keybinding t)
  (setq evil-respect-visual-line-mode t)
  (setq evil-want-fine-undo t) 
  (evil-mode 1))
(provide 'evil-rcp)
