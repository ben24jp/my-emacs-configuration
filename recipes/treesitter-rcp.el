(use-package tree-sitter-langs
  :ensure t
  :defer t)
(use-package tree-sitter
  :ensure t
  :defer t
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))
(provide 'treesitter-rcp)
