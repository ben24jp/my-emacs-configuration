(use-package highlight-indent-guides
  :ensure t
  :defer t
  :hook ((prog-mode text-mode conf-mode) . highlight-indent-guides-mode)
  :init
  (setq highlight-indent-guides-method 'character
        highlight-indent-guides-supress-auto-error t))
