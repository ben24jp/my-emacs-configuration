(use-package company
  :ensure t
  :defer t
  :hook
  ((prog-mode text-mode lisp-interaction-mode) . company-mode)
  :bind
  (:map company-active-map ("<tab>" . company-complete-common-or-cycle))
  :config
  (setq company-minimum-prefix-length 1)
  ;;(setq company-backends '(company-capf
  ;;                         company-yasnippet
  ;;                         company-keywords
  ;;                         company-files
  ;;                         company-elisp
  ;;                         company-ispell
  ;;                         company-semantic
  ;;                         company-dabbrev
  ;;                         company-dabbrev-code))
  ;;(setq company-tooltip-maximum-width 60)
  (company-mode 1)
  :custom
  (company-format-margin-function    #'company-vscode-dark-icons-margin)
  (company-idle-delay 0.1)
  (company-echo-delay 0.1))
(provide 'company-rcp)
