(use-package flyspell-correct
  :ensure t)

(dolist (hook '(text-mode-hook org-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))
(provide 'spellcheck-rcp)
