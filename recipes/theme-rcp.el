;; Theme
(use-package doom-themes
  :ensure t
  :config
  (load-theme 'doom-snazzy t)      ;; Setting theme name
  (doom-themes-visual-bell-config)      ;; Enabling flahing modeline for errors
  (doom-themes-org-config))             ;; Corrects org-mode's native fontification.

;; Modeline theme
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

;; Fonts
(defun daemon/set-font-faces ()
  ;; Transperent background
  (set-frame-parameter (selected-frame) 'alpha '(98 98))
  (add-to-list 'default-frame-alist '(alpha 98 98))
  
  ;; Setting default font
  (set-face-attribute 'default nil
                      :family "JetBrainsMono"
                      :weight 'regular
                      :foreground "white"
                      :height 120)

  ;; Setting fixed pitched font
  (set-face-attribute 'fixed-pitch nil
                      :family "JetBrainsMono"
                      :weight 'regular
                      :foreground "white"
                      :height 120)

;; Setting variable pitched font
  (set-face-attribute 'variable-pitch nil
                      :family "Iosevka Aile"
                      :weight 'regular
                      :foreground "white"
                      :height 120))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (setq doom-modeline-icon t)
                (with-selected-frame frame
                  (daemon/set-font-faces))))
  (daemon/set-font-faces))

(provide 'theme-rcp)
