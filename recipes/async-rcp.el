(use-package async
  :ensure t
  :config
  (autoload 'dired-async-mode "dired-async.el" nil 1)
  (dired-async-mode 1))
(provide 'async-rcp)
