(use-package rustic
  :ensure t
  :defer t
  :config
  (setq rustic-format-on-save 1)
  (setq rustic-lsp-server 'rls))
(provide 'rustic-rcp)
