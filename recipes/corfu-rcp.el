(use-package corfu
  :ensure t
  :defer t
  :init
  (corfu-global-mode)
  :custom
  (corfu-cycle t))
(provide 'corfu-rcp)
