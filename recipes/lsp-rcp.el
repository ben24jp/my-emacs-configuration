(use-package lsp-mode
  :ensure t
  :defer t
  :init (setq lsp-keymap-prefix "C-l"
              lsp-enable-file-watchers nil
              lsp-enable-on-type-formatting 1
              lsp-enable-snippet 1)
  :commands (lsp lsp-deferred)
  :hook (rustic-mode . lsp)
  :bind (:map lsp-mode-map ("TAB" . completion-at-point))
  :config
  (lsp-enable-which-key-integration t)
  :custom (lsp-headerline-breadcrumb-enable nil))

(use-package lsp-ui
  :ensure t
  :defer t
  :hook (lsp-mode . lsp-ui-mode)
  :commands lsp-ui-mode)
(provide 'lsp-rcp)
