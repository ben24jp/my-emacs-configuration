#+TITLE: My Emacs Configuration
#+OPTIONS: toc:nil num:nil

* Init.el
#+begin_src emacs-lisp :tangle init.el
(add-to-list 'load-path (expand-file-name "recipes/" (file-name-directory load-file-name)))
(require 'package-management-rcp)
(require 'async-rcp)
(require 'default-rcp)
(require 'theme-rcp)  
(require 'ligatures-rcp)
(require 'evil-rcp)
(require 'helpful-rcp)
(require 'counsel-rcp)
(require 'org-rcp)
(require 'org-roam-rcp)
(require 'ivy-rcp)
(require 'snippets-rcp)
(require 'company-rcp)
(require 'company-local-rcp)
(require 'lsp-rcp)
(require 'rustic-rcp)
(require 'ccls-rcp)
(require 'haskell-rcp)
(require 'python-rcp)
(require 'treesitter-rcp)
(require 'projectile-rcp)
(require 'keybinds-rcp)
(require 'which-key-rcp)
(require 'spellcheck-rcp)
(require 'email-rcp)
#+end_src

* default-rcp
#+begin_src emacs-lisp :tangle recipes/default-rcp.el
(setq inhibit-startup-message t)        ;; Disable startup messages
(scroll-bar-mode -1)                    ;; Disable the scroll bar
(tool-bar-mode -1)                      ;; Disable the tool bar
(tooltip-mode -1)                       ;; Disable tooltips
(set-fringe-mode 5)                     ;; Give some breathing room
(menu-bar-mode -1)                      ;; Diable menubar
(recentf-mode 0)
(electric-pair-mode 1)                  ;; Enable automatically adding pair brackets

(use-package rainbow-delimiters         ;; Enable color for pair brackets
  :ensure t
  :defer t
  :hook ((prog-mode org-src-mode) . rainbow-delimiters-mode)
  :config (rainbow-delimiters-mode t))

(use-package column-enforce-mode
  :ensure t
  :defer t
  :hook (prog-mode . column-enforce-mode)
  :config (80-column-rule))

(setq backup-directory-alist '(("." . "~/MyEmacsBackups")))

(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

(setq mouse-wheel-scroll-amount '(2 ((shift) . 1))) ;; Five line at a time
(setq mouse-wheel-progressive-speed nil)            ;; don't accelarate scrolling
(setq mouse-wheel-follow-mouse 't)                  ;; scroll window under mouse
(setq scroll-step 1)                                ;; Keyboard scrolling set to one line at a time

(column-number-mode 1)
(global-display-line-numbers-mode t)
(setq display-line-numbers-type 'relative)
;; Enable line numbers in specifiv modes
(dolist (mode '(text-mode-hook
    prog-mode-hook
    cond-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(setq-default indent-tabs-mode nil
    tab-width 4)

(setq-default tab-always-indent nil)

(setq tabify-regexp "^\t* [ \t]+")

(fset 'yes-or-no-p 'y-or-n-p)

(setq require-final-newline t)

(setq next-line-add-newlines nil)

(setq message-log-max 100)

(require 'paren) (show-paren-mode t)

(provide 'default-rcp)
#+end_src

* Keybinds
#+begin_src emacs-lisp :tangle recipes/keybinds-rcp.el
(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "M-s") 'flyspell-correct-at-point)
(provide 'keybinds-rcp)
#+end_src
* Package management
#+begin_src emacs-lisp :tangle recipes/package-management-rcp.el
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
  (package-refresh-contents))

(package-install 'use-package)

(provide 'package-management-rcp)
#+end_src

* Themes
#+begin_src emacs-lisp :tangle recipes/theme-rcp.el
;; Theme
(use-package doom-themes
  :ensure t
  :config
  (load-theme 'doom-snazzy t)      ;; Setting theme name
  (doom-themes-visual-bell-config)      ;; Enabling flahing modeline for errors
  (doom-themes-org-config))             ;; Corrects org-mode's native fontification.

;; Modeline theme
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

;; Fonts
(defun daemon/set-font-faces ()
  ;; Transperent background
  (set-frame-parameter (selected-frame) 'alpha '(98 98))
  (add-to-list 'default-frame-alist '(alpha 98 98))
  
  ;; Setting default font
  (set-face-attribute 'default nil
                      :family "JetBrainsMono"
                      :weight 'regular
                      :foreground "white"
                      :height 120)

  ;; Setting fixed pitched font
  (set-face-attribute 'fixed-pitch nil
                      :family "JetBrainsMono"
                      :weight 'regular
                      :foreground "white"
                      :height 120)

;; Setting variable pitched font
  (set-face-attribute 'variable-pitch nil
                      :family "Iosevka Aile"
                      :weight 'regular
                      :foreground "white"
                      :height 120))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (setq doom-modeline-icon t)
                (with-selected-frame frame
                  (daemon/set-font-faces))))
  (daemon/set-font-faces))

(provide 'theme-rcp)
#+end_src

* Evil
#+begin_src emacs-lisp :tangle recipes/evil-rcp.el
(use-package undo-tree
  :ensure t
  :init
  (setq evil-undo-system 'undo-tree)
  :config
  (setq undo-limit 400000           ; 400kb (default is 160kb)
        undo-strong-limit 3000000   ; 3mb   (default is 240kb)
        undo-outer-limit 48000000)  ; 48mb  (default is 24mb)
  (global-undo-tree-mode t))

(use-package evil
  :ensure t
  :defer t
  :init
  (setq evil-want-integration t)
  (setq evil-want-fine-undo t)
  (setq evil-want-keybinding t)
  (setq evil-respect-visual-line-mode t)
  (setq evil-want-fine-undo t) 
  (evil-mode 1))
(provide 'evil-rcp)
#+end_src

* Which key
#+begin_src emacs-lisp :tangle recipes/which-key-rcp.el
(use-package which-key
  :ensure t
  :defer t
  :init
  (setq which-key-sort-order #'which-key-key-order-alpha
        which-key-sort-uppercase-first nil
        which-key-add-column-padding 1
        which-key-max-display-columns nil
        which-key-min-display-lines 6
        which-key-side-window-slot -10)
  :hook (doom-first-input . which-key-mode)
  :config
  (setq which-key-idle-delay 0.1))
(which-key-mode)
(provide 'which-key-rcp)
#+end_src

* Counsel
  #+begin_src emacs-lisp :tangle recipes/counsel-rcp.el
(use-package counsel
  :ensure t
  :defer t
  :bind (("M-x" . counsel-M-x)
         ("<menu>" . counsel-M-x)
         ("C-x b" . counsel-switch-buffer)
         ("C-x C-f" . counsel-find-file)
         ("C-x d" . counsel-dired)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history))
  :config
  (setq ivy-initial-inputs-alist nil))
(provide 'counsel-rcp)
  #+end_src

* Helpful
#+begin_src emacs-lisp :tangle recipes/helpful-rcp.el
(use-package helpful
  :ensure t
  :defer t
  :bind
  ([remap describe-callable]    . helpful-callable)
  ([remap describe-function]    . helpful-function)
  ([remap describe-variable]    . helpful-variable)
  ([remap describe-key]         . helpful-key)
  ([remap view-emacs-debugging] . helpful-at-point))

(provide 'helpful-rcp)
#+end_src
* Indent Guide
#+begin_src emacs-lisp :tangle recipes/indent-guide-rcp.el
(use-package highlight-indent-guides
  :ensure t
  :defer t
  :hook ((prog-mode text-mode conf-mode) . highlight-indent-guides-mode)
  :init
  (setq highlight-indent-guides-method 'character
        highlight-indent-guides-supress-auto-error t))
#+end_src
* Snippets
#+begin_src emacs-lisp :tangle recipes/snippets-rcp.el
(use-package yasnippet-snippets                          
  :ensure t                                              
  :defer t)                                              
                                                         
(use-package yasnippet                                   
  :ensure t                                              
  :defer t                                               
  :init (setq yas-snippet-dirs '("~/.config/emacs/snippets"))
  :config
  (add-hook 'prog-mode-hook #'yas-minor-mode)
  (yas-reload-all))

(provide 'snippets-rcp)
#+end_src
* Org
#+begin_src emacs-lisp :tangle recipes/org-rcp.el
(defun efs/org-font-setup ()
  (font-lock-add-keywords 'org-mode
                          '(("^ *\\([-]\\) "
                             (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))

  (setq org-format-latex-options (plist-put org-format-latex-options :scale 2.0))
  
  ;; Set faces for heading levels
  (set-face-attribute 'org-document-title nil :font "Iosevka Aile" :weight 'bold :height 1.3)
  (dolist (face '((org-level-1 . 1.2)
                (org-level-2 . 1.1)
                (org-level-3 . 1.05)
                (org-level-4 . 1.0)
                (org-level-5 . 1.1)
                (org-level-6 . 1.1)
                (org-level-7 . 1.1)
                (org-level-8 . 1.1)))
  (set-face-attribute (car face) nil :font "Iosevka Aile" :weight 'regular :height (cdr face)))

;; Ensure that anything that should be fixed-pitch in Org files appears that way
(set-face-attribute 'org-block nil    :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
(set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil     :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-checkbox nil  :inherit 'fixed-pitch)
(set-face-attribute 'line-number nil :inherit 'fixed-pitch)
(set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch))

;;(setq org-latex-listings 'minted
;;      org-latex-packages-alist '(("" "minted"))
;;      org-latex-pdf-process
;;      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

;; Turn on indentation and auto-fill mode for Org files
(defun efs/org-mode-setup ()
      (org-indent-mode)
      (variable-pitch-mode 1)
      (auto-fill-mode 0)
      (visual-line-mode 1)
      (display-line-numbers-mode 0)
      (setq evil-auto-indent nil))

(use-package org
  :ensure t
  :defer t
  :hook (org-mode . efs/org-mode-setup)
  :config
  (setq org-ellipsis " ▾"
        org-hide-emphasis-markers t
        org-src-fontify-natively t
        org-fontify-quote-and-verse-blocks t
        org-src-tab-acts-natively 1
        org-edit-src-content-indentation 2
        org-hide-block-startup nil
        org-src-preserve-indentation 1
        org-startup-folded 'content)
  (efs/org-font-setup))

(defun efs/org-mode-visual-fill ()
  (setq visual-fill-column-width 115
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :ensure t
  :defer t
  :defer t
  :hook (org-mode . efs/org-mode-visual-fill))

(use-package org-bullets
  :ensure t
  :defer t
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src sh"))
(add-to-list 'org-structure-template-alist '("hs" . "src haskell"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("sc" . "src scheme"))
(add-to-list 'org-structure-template-alist '("ts" . "src typescript"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("go" . "src go"))
(add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))
(add-to-list 'org-structure-template-alist '("json" . "src json"))
(add-to-list 'org-structure-template-alist '("latex" . "src latex"))

(setq org-latex-create-formula-image-program 'dvisvgm)
(plist-put org-format-latex-options :justify 'center)

(use-package ox-pandoc
  :ensure t)

(provide 'org-rcp)
#+end_src

* Org roam
#+begin_src emacs-lisp :tangle recipes/org-roam-rcp.el
(use-package org-roam
  :ensure t
  :defer t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory "~/Documents/RoamNotes")
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert))
  :config
  (org-roam-setup))
(provide 'org-roam-rcp)
#+end_src
* Ivy
#+begin_src emacs-lisp :tangle recipes/ivy-rcp.el
(use-package ivy
  :ensure t
  :defer t
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-f" . ivy-alt-done)
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :init
  (ivy-mode 1)
  :config
  (setq ivy-use-virtual-buffers t)
  (setq ivy-wrap t)
  (setq ivy-count-format "(%d/%d) ")
  (setq enable-recursive-minibuffers t)

  ;; Use different regex strategies per completion command
  (push '(completion-at-point . ivy--regex-fuzzy) ivy-re-builders-alist) ;; This doesn't seem to work...
  (push '(swiper . ivy--regex-ignore-order) ivy-re-builders-alist)
  (push '(counsel-M-x . ivy--regex-ignore-order) ivy-re-builders-alist)

  ;; Set minibuffer height for different commands
  (setf (alist-get 'counsel-projectile-ag ivy-height-alist) 15)
  (setf (alist-get 'counsel-projectile-rg ivy-height-alist) 15)
  (setf (alist-get 'swiper ivy-height-alist) 15)
  (setf (alist-get 'counsel-switch-buffer ivy-height-alist) 7))

(use-package ivy-rich
  :ensure t
  :defer t
  :init
  (ivy-rich-mode 1))

(provide 'ivy-rcp)
#+end_src
* Company
#+begin_src emacs-lisp :tangle recipes/company-rcp.el
(use-package company
  :ensure t
  :defer t
  :hook
  ((prog-mode text-mode lisp-interaction-mode) . company-mode)
  :bind
  (:map company-active-map ("<tab>" . company-complete-common-or-cycle))
  :config
  (setq company-minimum-prefix-length 1)
  ;;(setq company-backends '(company-capf
  ;;                         company-yasnippet
  ;;                         company-keywords
  ;;                         company-files
  ;;                         company-elisp
  ;;                         company-ispell
  ;;                         company-semantic
  ;;                         company-dabbrev
  ;;                         company-dabbrev-code))
  ;;(setq company-tooltip-maximum-width 60)
  (company-mode 1)
  :custom
  (company-format-margin-function    #'company-vscode-dark-icons-margin)
  (company-idle-delay 0.1)
  (company-echo-delay 0.1))
(provide 'company-rcp)
#+end_src

* Company localized
#+begin_src emacs-lisp :tangle recipes/company-local-rcp.el
(defun company-emacs-lisp-mode ()
  "Set up `company-mode' for `emacs-lisp-mode'."
  (set (make-local-variable 'company-backends)
       '((company-yasnippet
          company-capf
          company-elisp
          company-dabbrev-code
          company-files))))

(defun company-rustic-mode ()
  "Set up `company-mode' for `rustic-mode'."
  (set (make-local-variable 'company-backends)
       '((company-yasnippet
          company-capf
          company-keywords
          company-dabbrev-code
          company-files))))

(defun company-cc-mode ()
  "Set up `company-mode' for `c-common-mode'."
  (set (make-local-variable 'company-backends)
       '((company-yasnippet
          company-capf
          company-keywords
          company-dabbrev-code
          company-files))))

(add-hook 'emacs-lisp-mode-hook 'company-emacs-lisp-mode)
(add-hook 'rustic-mode-hook 'company-rustic-mode)
(add-hook 'c-mode 'company-cc-mode)
(add-hook 'c++-mode 'company-cc-mode)

(provide 'company-local-rcp)
#+end_src
* Rustic
#+begin_src emacs-lisp :tangle recipes/rustic-rcp.el
(use-package rustic
  :ensure t
  :defer t
  :config
  (setq rustic-format-on-save 1)
  (setq rustic-lsp-server 'rls))
(provide 'rustic-rcp)
#+end_src
* CCLS
#+begin_src emacs-lisp :tangle recipes/ccls-rcp.el
(use-package ccls
  :ensure t
  :defer t
  :hook ((c-mode c++-mode objc-mode) . (lambda () (require 'ccls) (lsp)))
  :config (setq ccls-executable "/usr/bin/ccls"))
(provide 'ccls-rcp)
#+end_src
* Haskell
#+begin_src emacs-lisp :tangle recipes/haskell-rcp.el
(use-package lsp-haskell
  :ensure t
  :defer t
  :config
  (add-hook 'haskell-mode-hook #'lsp)
  (add-hook 'haskell-doc-mode #'lsp)
  (add-hook 'haskell-literate-mode #'lsp))
(provide 'haskell-rcp)
#+end_src
* Python
#+begin_src emacs-lisp :tangle recipes/python-rcp.el
(use-package lsp-pyright
  :ensure t
  :defer t
  :hook (python-mode . (lambda ()
                         (require 'lsp-pyright)
                         (lsp))))
(provide 'python-rcp)
#+end_src
* Treesitter
#+begin_src emacs-lisp :tangle recipes/treesitter-rcp.el
(use-package tree-sitter-langs
  :ensure t
  :defer t)
(use-package tree-sitter
  :ensure t
  :defer t
  :config
  (require 'tree-sitter-langs)
  (global-tree-sitter-mode)
  (add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode))
(provide 'treesitter-rcp)
#+end_src
* Lsp
#+begin_src emacs-lisp :tangle recipes/lsp-rcp.el
(use-package lsp-mode
  :ensure t
  :defer t
  :init (setq lsp-keymap-prefix "C-l"
              lsp-enable-file-watchers nil
              lsp-enable-on-type-formatting 1
              lsp-enable-snippet 1)
  :commands (lsp lsp-deferred)
  :hook (rustic-mode . lsp)
  :bind (:map lsp-mode-map ("TAB" . completion-at-point))
  :config
  (lsp-enable-which-key-integration t)
  :custom (lsp-headerline-breadcrumb-enable nil))

(use-package lsp-ui
  :ensure t
  :defer t
  :hook (lsp-mode . lsp-ui-mode)
  :commands lsp-ui-mode)
(provide 'lsp-rcp)
#+end_src

* Projectile
#+begin_src emacs-lisp :tangle recipes/projectile-rcp.el
(use-package projectile
  :ensure t
  :commands projectile-project-root
  :defer 2
  :defines projectile-globally-ignored-directories
  :config
  (projectile-mode)
  (add-to-list 'projectile-globally-ignored-directories "*bloop")
  (add-to-list 'projectile-globally-ignored-directories "*metals")
  :bind
  ("C-c p" . projectile-command-map))

(provide 'projectile-rcp)
#+end_src

* Ligatures
#+begin_src emacs-lisp :tangle recipes/ligatures-rcp.el
(defvar ligatures-JetBrainsMono-text
  '("--" "---" "==" "===" "!=" "!==" "=!=" "=:=" "=/=" "<=" ">=" "&&" "&&&" "&=" "++" "+++"
    "***" ";;" "!!" "??" "?:" "?." "?=" "<:" ":<" ":>" ">:" "<>" "<<<" ">>>" "<<" ">>" "||" "-|"
    "_|_" "|-" "||-" "|=" "||=" "##" "###" "####" "#{" ":[" "]#" "#(" "#?" "#_" "#_(" "#:"
    "#!" "#=" "^=" "<$>" "<$" "$>" "<+>" "<+ +>" "</" "</>" "/>" "<!--"
    "<#--" "-->" "->" "->>" "<<-" "<-" "<=<" "=<<" "<<=" "<==" "<=>" "<==>" "==>" "=>"
    "=>>" ">=>" ">>=" ">>-" ">-" ">--" "-<" "-<<" ">->" "<-<" "<-|" "<=|" "|=>" "|->" "<-"
    "<~~" "<~" "<~>" "~~" "~~>" "~>" "~-" "-~" "~@" "[||]" "|]" "[|" "|}" "{|" "[< >]"
    "|>" "<|" "||>" "<||" "|||>" "<|||" "<|>" "..." ".." ".=" ".-" "..<" ".?" "::" ":::"
    ":=" "::=" ":?" ":?>" "//" "///" "/*" "*/" "/=" "//=" "/==" "@_" "__"))

(defvar ligatures-JetBrainsMono-rust
  '("--" "---" "==" "===" "!=" "!==" "=!=" "=:=" "=/=" "<=" ">=" "&&" "&&&" "&=" "++" "+++"
    "***" ";;" "!!" "??" "?:" "?." "?=" "<:" ":<" ":>" ">:" "<>" "<<<" ">>>" "<<" ">>" "||" "-|"
    "_|_" "|-" "||-" "|=" "||=" "##" "###" "####" "#{" ":[" "]#" "#(" "#?" "#_" "#_(" "#:"
    "#!" "#=" "^=" "<$>" "<$" "$>" "<+>" "<+ +>" "</" "</>" "/>" "<!--"
    "<#--" "-->" "->" "->>" "<<-" "<-" "<=<" "=<<" "<<=" "<==" "<=>" "<==>" "==>" "=>"
    "=>>" ">=>" ">>=" ">>-" ">-" ">--" "-<" "-<<" ">->" "<-<" "<-|" "<=|" "|=>" "|->" "<-"
    "<~~" "<~" "<~>" "~~" "~~>" "~>" "~-" "-~" "~@" "[||]" "|]" "[|" "|}" "{|" "[< >]"
    "|>" "<|" "||>" "<||" "|||>" "<|||" "<|>" "..." ".." ".=" ".-" "..<" ".?" "::" ":::"
    ":=" "::=" ":?" ":?>" "//" "///" "/*" "*/" "/=" "//=" "/==" "@_" "__"))

;; These are the ligatures for python-mode
(defvar ligatures-JetBrainsMono-python
  '("--" "---" "==" "===" "!=" "!==" "=!=" "=:=" "<=" ">=" "&&" "&&&" "&=" "++" "+++"
    "***" ";;" "!!" "??" "?:" "?." "<:" ":<" ":>" ">:" "<>" "<<<" ">>>" "<<" ">>" "||" "-|"
    "_|_" "|-" "||-" "|=" "||=" "^=" "<$>" "<$" "$>" "<+>" "<+ +>" "<*>" "<* *>" "</" "</>" "/>" "<!--"
    "-->" "->" "->>" "<<-" "<-" "<=<" "=<<" "<<=" "<==" "<=>" "<==>" "==>" "=>"
    "=>>" ">=>" ">>=" ">>-" ">-" ">--" "-<" "-<<" ">->" "<-<" "<-|" "<=|" "|=>" "|->" "<-"
    "<~~" "<~" "<~>" "~~" "~~>" "~>" "~-" "[||]" "|]" "[|" "|}" "{|" "[<" ">]"
    "|>" "<|" "||>" "<||" "|||>" "<|>" ".=" "..<" ".?" "::" ":::"
    ":=" "::=" ":?" ":?>" "/=" "//=" "/==" "__"))

(defvar ligatures-JetBrainsMono-cc
  '("--" "---" "==" "===" "!=" "!==" "=!=" "=:=" "=/=" "<=" ">=" "&&" "&&&" "&=" "++" "+++"
    ;;"***" ";;" "!!" "??" "?:" "?." "?=" "<:" ":<" ":>" ">:" "<>" "<<<" ">>>" "<<" ">>" "||" "-|"
    ;;"_|_" "|-" "||-" "|=" "||=" "##" "###" "####" "#{" ":[" "]#" "#(" "#?" "#_" "#_(" "#:"
    ;;"#!" "#=" "^=" "<$>" "<$" "$>" "<+>" "<+ +>" "</" "</>" "/>" "<!--"
    ;;"<#--" "-->" "->" "->>" "<<-" "<-" "<=<" "=<<" "<<=" "<==" "<=>" "<==>" "==>" "=>"
    ;;"=>>" ">=>" ">>=" ">>-" ">-" ">--" "-<" "-<<" ">->" "<-<" "<-|" "<=|" "|=>" "|->" "<-"
    ;;"<~~" "<~" "<~>" "~~" "~~>" "~>" "~-" "-~" "~@" "[||]" "|]" "[|" "|}" "{|" "[< >]"
    ;;"|>" "<|" "||>" "<||" "|||>" "<|||" "<|>" "..." ".." ".=" ".-" "..<" ".?" "::" ":::"
    ;;":=" "::=" ":?" ":?>" "//" "///" "/*" "*/" "/=" "//=" "/==" "@_" "__"
    ))

(use-package ligature
  ;; git repo [https://github.com/mickeynp/ligature.el.git]
  :load-path "/home/simson/.config/emacs/ligature.el"
  :config
  (ligature-set-ligatures 'python-mode ligatures-JetBrainsMono-python)
  (ligature-set-ligatures 'rustic-mode ligatures-JetBrainsMono-rust)
  (ligature-set-ligatures 'text-mode ligatures-JetBrainsMono-rust)
  (ligature-set-ligatures 'emacs-lisp-mode ligatures-JetBrainsMono-rust)
  (ligature-set-ligatures 'c-mode ligatures-JetBrainsMono-cc)
  (ligature-set-ligatures 'c++-mode ligatures-JetBrainsMono-cc)
  (global-ligature-mode 1))

(provide 'ligatures-rcp)
#+end_src

* Spelling checking
#+begin_src emacs-lisp :tangle recipes/spellcheck-rcp.el
(use-package flyspell-correct
  :ensure t)

(dolist (hook '(text-mode-hook org-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))
(provide 'spellcheck-rcp)
#+end_src

* Asynchronous processing in Emacs
#+begin_src emacs-lisp :tangle recipes/async-rcp.el
(use-package async
  :ensure t
  :config
  (autoload 'dired-async-mode "dired-async.el" nil 1)
  (dired-async-mode 1))
(provide 'async-rcp)
#+end_src

* Email
#+begin_src emacs-lisp :tangle recipes/email-rcp.el
(use-package mu4e
  :load-path "/usr/share/emacs/site-lisp/mu4e/"
  :config
  (setq mu4e-change-filenames-when-moving t)
  (setq mu4e-update-interval (* 10 60))
  (setq mu4e-get-mail-command "mbsync -a")
  (setq mu4e-maildir "~/Mail")

  (setq mu4e-drafts-folder "/[Gmail]/Drafts")
  (setq mu4e-sent-folder   "/[Gmail]/Sent Mail")
  (setq mu4e-refile-folder "/[Gmail]/All Mail")
  (setq mu4e-trash-folder  "/[Gmail]/Trash")

  (setq mu4e-maildir-shortcuts
        '(("/Inbox"             . ?i)
          ("/[Gmail]/Sent Mail" . ?s)
          ("/[Gmail]/Trash"     . ?t)
          ("/[Gmail]/Drafts"    . ?d)
          ("/[Gmail]/All Mail"  . ?a)
          ("/[Gmail]/Important" . ?m)
          ("/[Gmail]/Stared"    . ?p))))

(provide 'email-rcp)
#+end_src

